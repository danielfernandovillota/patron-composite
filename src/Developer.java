public class Developer implements Empleado {

    private String nombre;
    private double salario;

    public Developer(String nombre, double salario){
        this.nombre=nombre;
        this.salario=salario;
    }
    @Override
    public void add(Empleado empleado) {

    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public double getSalario() {
        return salario;
    }

    @Override
    public void print() {
        System.out.println("----------------");
        System.out.println("nombre: "+getNombre());
        System.out.println("salario: "+getSalario());
    }
}
