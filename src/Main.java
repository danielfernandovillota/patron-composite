public class Main {
    public static void main(String[] args){
        Empleado empleado1=new Developer("daniel",2000000);
        Empleado empleado2=new Developer("juan", 1500000);
        Empleado gerente1=new ManagerComposite("Felipe",3000000);
        gerente1.add(empleado1);
        gerente1.add(empleado2);
        Empleado gerenteGeneral=new ManagerComposite("Ana", 4000000);
        gerenteGeneral.add(gerente1);
        gerenteGeneral.print();

    }
}
