import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ManagerComposite implements Empleado {

    private String nombre;
    private double salario;

    public ManagerComposite(String nombre, double salario){
        this.nombre=nombre;
        this.salario=salario;
    }

    List<Empleado> empleados=new ArrayList<Empleado>();

    @Override
    public void add(Empleado empleado) {
        empleados.add(empleado);
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public double getSalario() {
        return salario;
    }

    @Override
    public void print() {
        System.out.println("----------------");
        System.out.println("nombre: "+getNombre());
        System.out.println("salario: "+getSalario());

        Iterator<Empleado> empleadoIterator=empleados.iterator();
        while (empleadoIterator.hasNext()){
            Empleado empleado=empleadoIterator.next();
            empleado.print();
        }
    }
}
